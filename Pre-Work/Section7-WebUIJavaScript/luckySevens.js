var bet = document.getElementById("bet");
var play = document.getElementById("roll");
var table = document.getElementsByClassName("table")[0];
var tableStartBet = document.getElementById("startBet");
var tableRollCount = document.getElementById("rollCount");
var tableMaxEarned = document.getElementById("maxEarned");
var tableMaxCount = document.getElementById("maxCount");
function rollDie() {
    return Math.floor(Math.random()*6)+1;
}

function gamble() {
    var betValue = parseInt(bet.value),
        rollCount = 0,
        resultOne = 0,
        resultTwo = 0,
        resultTotal = 0,
        maxEarned = betValue,
        maxCount = 0;
        tableStartBet.textContent = ("$" + betValue);
    while (betValue > 0) {
        resultOne = rollDie();
        resultTwo = rollDie();
        resultTotal = (resultOne + resultTwo);
        rollCount++;
        betValue = (resultTotal === 7) ? betValue += 4 : betValue -= 1;
        if (betValue >= maxEarned) {
            maxEarned = betValue;
            maxCount = rollCount;
        }
    }
    tableRollCount.textContent = rollCount;
    tableMaxEarned.textContent = ("$" + maxEarned);
    tableMaxCount.textContent = maxCount;
    table.style.display = "table";
    play.value = "Play Again?";
}
play.addEventListener("click",gamble);