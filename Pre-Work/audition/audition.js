
window.onload=function(){
var start = document.getElementById("starting");
var evens = document.getElementById("numEvens");
var textArea = document.getElementById("text");
var button = document.getElementById("button");
var arr = [];
var currentValue;

function getEvens(){
    var textString = `Starting with ${start.value}, the next ${evens.value} even numbers are`;
    currentValue = (parseInt(start.value) +1);
    while (arr.length<parseInt(evens.value)){
        if ((currentValue)%2===0){
            arr.push(currentValue);
        }
        currentValue++;
    }
    for(var i=0;i<(arr.length-1);i++){
        textString += ` ${arr[i]},`;
    }
    textString+=` and ${arr[arr.length-1]}`;
    textArea.innerText = textString;
}
button.addEventListener("click",getEvens);
}